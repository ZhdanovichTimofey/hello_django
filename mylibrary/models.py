from django.db import models

# Create your models here.


class Shelf(models.Model):
    name = models.CharField('Название полки', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Полка'
        verbose_name_plural = 'Полки'


class Book(models.Model):
    shelf = models.CharField(default='Полка', max_length=100)
    name = models.CharField(default='Название книги', max_length=1000)
    author = models.CharField(default='Автор', max_length=1000)
    content = models.TextField(default='Содержание', max_length=2000)
    is_taken = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'
