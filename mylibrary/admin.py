from django.contrib import admin
from  .models import Shelf, Book
# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'shelf', 'author', 'is_taken')

admin.site.register(Shelf)
admin.site.register(Book, BookAdmin)


