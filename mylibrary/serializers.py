from rest_framework import serializers
from mylibrary.models import Book, Shelf


class BookSerializer_id(serializers.Serializer):
    id = serializers.IntegerField(read_only=False)


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['shelf', 'name', 'author', 'content', 'is_taken', 'id']


class BookSerializerList(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['name', 'id']


class ShelfSerializer(serializers.Serializer):
    name = serializers.CharField(required=False, max_length=100)

    """def selfbooks(name):
        books = Book.objects.filter(name=name)
        return BookSerializerList(books, many=True)

    books = selfbooks(name)"""
