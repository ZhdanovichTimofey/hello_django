from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from mylibrary.models import Book, Shelf
import mylibrary.serializers


@api_view(['POST'])
def book_take(request, format=None):
    if request.method == 'POST':
        serializer = mylibrary.serializers.BookSerializer_id(data=request.data)
        if serializer.is_valid():
            book = Book.objects.get(pk=serializer.data['id'])
        if book.is_taken == False:
            book.is_taken = True
            book.save()
            serializer = mylibrary.serializers.BookSerializer(book)
            return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def book_put(request, format=None):
    if request.method == 'POST':
        serializer = mylibrary.serializers.BookSerializer_id(data=request.data, partial=True)
    if serializer.is_valid():
        book = Book.objects.get(pk=serializer.data['id'])
    if book.is_taken == True:
        book.is_taken = False
        book.save()
        serializer = mylibrary.serializers.BookSerializer(book)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT'])
def book_list(request, format=None):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        books = Book.objects.all()
        serializer = mylibrary.serializers.BookSerializer(books, many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        book = Book.objects.create()
        book.save()
        serializer = mylibrary.serializers.BookSerializer(book, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'DELETE', 'PATCH'])
def book_detail(request, pk, format=None):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        book = Book.objects.get(pk=pk)
    except Book.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = mylibrary.serializers.BookSerializer(book)
        return Response(serializer.data)

    elif request.method == 'PATCH':
        serializer = mylibrary.serializers.BookSerializer(book, data=request.data, partial=True)
        if serializer.is_valid():
            book = serializer
            book.save()
        return Response(serializer.data)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', ])
def shelfs_list(request):
    if request.method == 'GET':
        shelfs = Shelf.objects.all()
        serializershelfs = mylibrary.serializers.ShelfSerializer(shelfs, many=True)
    return Response(serializershelfs.data)
