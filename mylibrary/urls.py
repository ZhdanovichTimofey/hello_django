from django.urls import path
from mylibrary import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('books/', views.book_list),
    path('books/<int:pk>/', views.book_detail),
    path('shelfs/', views.shelfs_list),
    path('books/take', views.book_take),
    path('books/put', views.book_put)
]

urlpatterns = format_suffix_patterns(urlpatterns)